import java.util.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

class ColectieMedicamente{
    private String l;
    private TreeSet<Medicament> lista; //lista este un arobre de medicamente
    private static ColectieMedicamente cc=new ColectieMedicamente();
    
    private ColectieMedicamente(){
        lista=new TreeSet(new MedicamentComparator()); //creezi un nou arbore cu paramentru clasa MedicamentComparator
    };
    static ColectieMedicamente getInstanta(){ 
        return cc;
    }

//functia ce cauta si returneaza un obiect dupa nume
    public Medicament cautaMedicamentNou(String nume){
        Iterator it=lista.iterator();
        while(it.hasNext()){
            Medicament m=(Medicament)it.next();
            if (m.getNume().equalsIgnoreCase(nume))
                return m;
        }
        return null;
    }
    public Medicament cautaMedicament(String nume){
        Iterator it=lista.iterator();
        while(it.hasNext()){
            Medicament m=(Medicament)it.next();
            if (m.getNume().equalsIgnoreCase(nume)){
                if (m.getNr()!=0) return m;
                else {
                    Mesaj.afiseaza("Eroare! Medicamentul "+nume+" nu este in stoc!","eroare");
                    Medicament m2=cautaMedicamentCompozitie(m);
                    Mesaj.afiseaza("Medicamentul "+m2.getNume()+" are aceeasi compozitie!","info");
                    return m2;
                }
            }
        }
        return null;
    }
//cauta medicament dupa compozitie
    public Medicament cautaMedicamentCompozitie(Medicament m){
        Iterator it=lista.iterator();
        while(it.hasNext()){
            Medicament m2=(Medicament)it.next();
            for(int i=0;i<m.getCompozitie().length;i++)
            if (m2.getCompozitie()[i].equals(m.getCompozitie()[i]) && m!=m2)
                return m2;
        }
        return null;
    }
//citeste medicamentele dintr-un fisier
    public void medicamenteFisier() throws Exception{
        try{
         BufferedReader buf=new BufferedReader(new FileReader ("src/Medicamente.txt"));
         String tmp=buf.readLine();
         Medicament m;
         while(tmp!=null){
                String linie[]=tmp.split(" ");
                m=new Medicament(linie[0],linie[1].split("_"),linie[2].split("_"),linie[3].split("_"),linie[4],Double.parseDouble(linie[5]));
                if(linie[6].equals("true"))
                    m.setCompensatie(true);
                else m.setCompensatie(false);
                m.setNr(Integer.parseInt(linie[7]));
                lista.add(m);
                tmp=buf.readLine();
         }
         buf.close();
        }catch(IOException e){Mesaj.afiseaza("Eroare la citirea din fisier!" +e.getMessage(), "eroare");}
           
    }
//adauga medicament nou
    public void adaugaMedicament(Medicament m){
        lista.add(m);
        String compensat=Mesaj.getData("Compensat?true/false");
        if(compensat.equalsIgnoreCase("true"))
            m.setCompensatie(true);
        else m.setCompensatie(false);
        m.setNr(Integer.parseInt(Mesaj.getData("Numarul de exemplare")));
        Mesaj.afiseaza("Medicamentul a fost adaugat!","info");
    }
//elimina un medicament din stoc
    public void eliminaMedicament(Medicament m){
        m.setNr(m.getNr()-1);
        Mesaj.afiseaza("Medicamentul "+m.getNume()+" a fost eliminat!","info");
    }
//sterge un medicament din colectie
    public void removeMedicament(String nume){
        Iterator it=lista.iterator();
        while(it.hasNext()){
            Medicament m=(Medicament)it.next();
            if (m.getNume().equalsIgnoreCase(nume)){
                lista.remove(m);
            }
        }
    }
//salveaza colectia
     public void salveaza(){
        try{
         PrintWriter out=new PrintWriter(new FileWriter ("src/Medicamente.txt"));
          Iterator it=lista.iterator();
         while(it.hasNext()){
            Medicament m=(Medicament)it.next();
            out.println(m.toString());
         }
         out.close();
        }catch(IOException e){Mesaj.afiseaza("Eroare la scriere in fisier!" +e.getMessage(), "eroare");}
    }

//returneaza un string cu datele medicamentului
    public String getMedicament(){
        l="";
        Iterator it=lista.iterator();
        while(it.hasNext()){
            Medicament m=(Medicament)it.next();
            l+=m.getInformatii()+"\r\n";
        }
        return l;
    }
}