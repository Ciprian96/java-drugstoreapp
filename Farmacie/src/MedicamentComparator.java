
import java.util.Comparator;
//compara medicamentele dupa nume si le sorteaza alfabetic
class MedicamentComparator implements Comparator<Medicament>{
    public int compare(Medicament m1,Medicament m2){
        return m1.getNume().compareTo(m2.getNume());
    }
}