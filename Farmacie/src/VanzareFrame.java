import java.awt.*;
import java.awt.event.*;

class VanzareFrame extends Frame{
    private AscultatorC asc;
    private Choice cReteta,cBoala;
    private Label lNume,lCost,lReteta,lBoala; 
    private TextField tNume,tCost;
    private Button bAdauga,bTermina;
    private Double cost=0.0;
    
    VanzareFrame(){
        super ("Farmacie");
        AscultatorC ac=new AscultatorC();
        
        lNume=new Label("Denumire medicament");
        lReteta=new Label("Cu sau fara reteta");
        lBoala=new Label("Boala de care suferiti");
        lCost=new Label("Cost actual");
        
        cReteta=new Choice();
        cReteta.add("Cu reteta");
        cReteta.add("Fara reteta");
        
        cBoala=new Choice();
        cBoala.add("Cancer");
        cBoala.add("Mintala");
        cBoala.add("alta...");
        
        tNume=new TextField(10);
        tCost=new TextField(6);
        tCost.setEditable(false);
        
        bAdauga=new Button("Adauga Medicament");
        bAdauga.addActionListener(ac);        
        bTermina=new Button("Incheie Tranzactia");
        bTermina.addActionListener(ac);        
        
        Panel p=new Panel();
        p.setLayout(new GridLayout(5,5));
        p.add(lReteta);p.add(cReteta);
        p.add(lBoala);p.add(cBoala);
        p.add(lNume);p.add(tNume);
        p.add(lCost);p.add(tCost);
        p.add(bAdauga);p.add(bTermina);
        

        add(p,BorderLayout.CENTER);
        
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
               addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent ev){dispose();}});
    }
    class AscultatorC implements ActionListener{
        public void actionPerformed(ActionEvent evt){
            if (evt.getSource()==bAdauga){
               cReteta.setEnabled(false);
               cBoala.setEnabled(false);
               String nume=tNume.getText();
               tNume.setText("");
               Medicament m=ColectieMedicamente.getInstanta().cautaMedicament(nume);
               if (m!=null){
               if(cReteta.getSelectedItem().equals("Cu reteta")){
                   if(m.isCompensatie()==true){
                       m.setPretCompensat(cBoala.getSelectedItem());
                       cost+=m.getPretCompensat();
                       tCost.setText(String.valueOf(cost));
                       ColectieMedicamente.getInstanta().eliminaMedicament(m);
                   }
                   else if(m.isCompensatie()==false){
                       cost+=m.getPretVanzare();
                       tCost.setText(String.valueOf(cost));
                       ColectieMedicamente.getInstanta().eliminaMedicament(m);
                   }
                   
               }
               else if(cReteta.getSelectedItem().equals("Fara reteta")){
                   cost+=m.getPretVanzare();
                   tCost.setText(String.valueOf(cost));
                   ColectieMedicamente.getInstanta().eliminaMedicament(m);
               }
            }else
                Mesaj.afiseaza("Medicamentul cu numele "+nume+" nu exista in baza de date!", "eroare");
            }
            else if (evt.getSource()==bTermina){
               Mesaj.afiseaza("Va costa "+tCost.getText()+" lei", "info");
               dispose();
            }
        }    
     }
    
}