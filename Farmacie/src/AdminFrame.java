import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPasswordField;


class AdminFrame extends Frame{
     private Button bSchimba,bElimina;
     private Choice cNume;
     private Label lNume;
    
    AdminFrame(){
        super ("Admin Panel");
        AscultatorCNF acnf=new AscultatorCNF();
        
        bSchimba=new Button("Schimba pretul");
        bSchimba.addActionListener(acnf);
        bElimina=new Button("Elimina din baza de date");
        bElimina.addActionListener(acnf);
        
        
        cNume=new Choice();
        try{
         BufferedReader buf=new BufferedReader(new FileReader ("src/Medicamente.txt"));
         String tmp=buf.readLine();
         while(tmp!=null){
                String linie[]=tmp.split(" ");
                cNume.add(linie[0]);
                tmp=buf.readLine();
         }
         buf.close();
        }catch(IOException e){Mesaj.afiseaza("Eroare la citirea din fisier!" +e.getMessage(), "eroare");}
        
        lNume=new Label("Denumire Medicament");
        
        Panel p1=new Panel();
        p1.setLayout(new GridLayout(2,2));
        p1.add(lNume);p1.add(cNume);
        p1.add(bSchimba);p1.add(bElimina);
        
        add(p1,BorderLayout.NORTH);

        
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent ev){dispose();}});
    }
    class AscultatorCNF implements ActionListener{
        public void actionPerformed(ActionEvent evt){
                if(evt.getSource()==bSchimba){
                    Medicament m=ColectieMedicamente.getInstanta().cautaMedicamentNou(cNume.getSelectedItem());
                    Double pret=Double.parseDouble(Mesaj.getData("Introduceti pretul"));
                    m.schimbaPret(pret);
                    Mesaj.afiseaza("Pretul medicamentului "+m.getNume()+" a fost modificat", "info");
                    dispose();
                }
                if(evt.getSource()==bElimina){
                    ColectieMedicamente.getInstanta().removeMedicament(cNume.getSelectedItem());
                    Mesaj.afiseaza("Medicamentul "+cNume.getSelectedItem()+" a fost sters din baza de date" , "info");
                    dispose();
                }
                    
        }
    }
}