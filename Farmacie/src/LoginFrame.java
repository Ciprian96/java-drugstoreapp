import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPasswordField;


class LoginFrame extends Frame{
     private Button bLogin;
     private TextField tUser;
     private Label lUser,lPass;
     JPasswordField tPass;
     String nume;
    
    LoginFrame(){
        super ("Admin Login");
        AscultatorCNF acnf=new AscultatorCNF();
        
        bLogin=new Button("Adauga");
        bLogin.addActionListener(acnf);
        
        tUser=new TextField(20);
        tPass=new JPasswordField(10);
        lUser=new Label("Username:");
        lPass=new Label("Password:");
        
        Panel p1=new Panel();
        p1.setLayout(new GridLayout(2,2));
        p1.add(lUser);p1.add(tUser);
        p1.add(lPass);p1.add(tPass);
        
        Panel p2=new Panel();
        p2.add(bLogin);
        
        add(p1,BorderLayout.NORTH);
        add(p2,BorderLayout.CENTER);
        
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent ev){dispose();}});
    }
    class AscultatorCNF implements ActionListener{
        public void actionPerformed(ActionEvent evt){
                if(evt.getSource()==bLogin)
                    if(tUser.getText().equals("admin") && String.valueOf(tPass.getPassword()).equals("admin"))
                        new AdminFrame();
                    else Mesaj.afiseaza("Username sau parola gresita!", "eroare");
        }
    }
}