import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class MedicamenteFrame extends Frame{
     private TextArea taLista;
     private Label lLista;
    
    MedicamenteFrame(){
        super ("Lista medicamentelor");

        
       
       taLista=new TextArea(30,50);
       taLista.setEditable(false);
       lLista=new Label("Toate medicamentele");
       taLista.setText(ColectieMedicamente.getInstanta().getMedicament());
        
        add(lLista,BorderLayout.NORTH);
        add(taLista,BorderLayout.CENTER);
        
        pack();
        setExtendedState(Frame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        setVisible(true);
        addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent ev){dispose();}});
    }
}