
import javax.swing.JOptionPane;
//afiseaza ferestre cu mesaj
class Mesaj{
    public static void afiseaza(String mesaj,String tipMesaj){
        if (tipMesaj.equals("info"))
            JOptionPane.showMessageDialog(null, mesaj,"Info", JOptionPane.INFORMATION_MESSAGE);
        if (tipMesaj.equals("eroare"))
            JOptionPane.showMessageDialog(null, mesaj,"Eroare", JOptionPane.ERROR_MESSAGE);
    }
    public static String getData(String mesaj){
        return JOptionPane.showInputDialog(mesaj);
    }
}