import java.awt.*;
import java.awt.event.*;

//clasa fereastra extinde clasa "Frame" din java

class Fereastra extends Frame{

//denumirea variabilelor si tipul lor

    private AscultatorC asc;
    private Button bNou,bAfis,bVanzare,bInchide,bAdmin;

//contructorul
    
    Fereastra() throws Exception{
        super ("Farmacie");

        AscultatorC ac=new AscultatorC(); //nou obiect de tip AscultatorC
        ColectieMedicamente.getInstanta().medicamenteFisier(); //citeste medicamentele din fisier
        
        bAdmin=new Button("Admin"); //nou obiect de tip Button
        bAdmin.addActionListener(ac); //adaug functionalitate pe buton cu parametru AscultatorC ac
        
        bNou=new Button("Aprovizionare Magazin");
        bNou.addActionListener(ac);
        
        bAfis=new Button("Lista Medicamente");
        bAfis.addActionListener(ac);
        
        bVanzare=new Button("Vanzare Medicamente");
        bVanzare.addActionListener(ac);
        
        bInchide=new Button("Inchide Aplicatia");
        bInchide.addActionListener(ac);
        
        Panel p=new Panel(); //nou panou
        p.setLayout(new GridLayout(2,2)); // layout de tip grid 2 linii 2 coloane
        p.add(bNou);p.add(bAfis);
        p.add(bVanzare);p.add(bInchide); // adaug obiectele in panou
        
	//adaug obiectele in frame

        add(bAdmin,BorderLayout.NORTH);
        add(p,BorderLayout.CENTER);
	        
        pack(); //dimensiunea frame-ului
        setLocationRelativeTo(null); //se localizeaza in centrul ecranului
        setVisible(true); //este vizibil

	//cand apesi pe close se salveaza modificarile in fisier si se inchide fereastra
        addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent ev){
            ColectieMedicamente.getInstanta().salveaza();
            System.exit(0);}});
    }
//clasa AscultatorC implementeaza clasa ActionListener din java
    class AscultatorC implements ActionListener{
	//functia actionPerformed primeste ca parametru un obiect de tip ActionEvent
        public void actionPerformed(ActionEvent evt){
	    //daca apesi pe butonul bAdmin
            if(evt.getSource()==bAdmin)
                new LoginFrame(); // se deschide fereastra LoginFrame
            if (evt.getSource()==bNou){
               String nume; 
               int numar;
               try{
                  do{
                    nume=Mesaj.getData("Introduceti nume medicament"); //se deschide o fereastra unde introduci numele
                    if(nume.equals("")) Mesaj.afiseaza("Nu ati introdus nici un nume", "eroare");
                  }while(nume.equals(""));
                    Medicament m=ColectieMedicamente.getInstanta().cautaMedicamentNou(nume); //cauta medicamentul dupa nume in colectie
		    //daca exista deja cresti stocul cu o anumita cantitate  
                    if (m!=null){
                        numar=Integer.parseInt(Mesaj.getData("Introduceti numarul de produse"));
                        m.setNr(m.getNr()+numar);
                        Mesaj.afiseaza("S-au adaugat in stoc "+numar+" elemente de "+m.getNume(), "info");
                    }
                    else
                        new MedicamentNouFrame(nume); //adaugi un nou medicament in colectie
                }catch(Exception e){Mesaj.afiseaza("Eroare la preluarea numelui!", "eroare");}
               
            }
            else if (evt.getSource()==bAfis)
                new MedicamenteFrame(); //se deschide fereastra MedicamentFrame
            else if (evt.getSource()==bVanzare)
                new VanzareFrame(); //se deschide fereastra VanzareFrame
            else if (evt.getSource()==bInchide){
                ColectieMedicamente.getInstanta().salveaza();
                System.exit(0); //se inchide fereastra si se salveaza modificarile
            }
        }    
     }
    
}