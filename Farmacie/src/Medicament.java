
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
//clasa medicamentului cu toate datele acestuia
class Medicament{
    private String nume;
    private String modAdministrare;
    protected String[] compozitie;
    private String[] indicatii;
    private String[] contraIndicatii;
    private Double pret,pretVanzare,pretCompensat;
    protected int nr;
    private boolean compensatie;
    Medicament(String nume,String[] compozitie,String[] indicatii,String[] contraIndicatii,String modAdministrare,Double pret)throws Exception{
            this.nume=nume;
            this.compozitie=compozitie;
            this.indicatii=indicatii;
            this.contraIndicatii=contraIndicatii;
            this.modAdministrare=modAdministrare;
            this.pret=pret;
            pretVanzare();
    }

    public Double getPretVanzare() {return pretVanzare;}    
    public String getNume() {return nume;}
    public String getModAdministrare() {return modAdministrare;}
    public String[] getCompozitie() {return compozitie;}
    public String[] getIndicatii() {return indicatii;}
    public String[] getContraIndicatii() {return contraIndicatii;}
    public Double getPret() {return pret;}
    public Double getPretCompensat(){return pretCompensat;}
    public int getNr() {return nr;}
    public void setNr(int nr) {this.nr = nr;}
    public boolean isCompensatie() {return compensatie;}
    public void setCompensatie(boolean compensatie) {this.compensatie = compensatie;}
    
    public void setPretCompensat(String boala){
        if(boala.equalsIgnoreCase("cancer")){
            pretCompensat=pretVanzare-(0.9*pretVanzare);
        }
        else if(boala.equalsIgnoreCase("mintala")){
            pretCompensat=0.0;
        }
        else pretCompensat=pretVanzare-(0.4*pretVanzare);
    }
    public void pretVanzare(){
        pretVanzare=pret+pret*0.1;
    }
    public void schimbaPret(Double pret){
        this.pret=pret;
        pretVanzare();
        
    }
    public String toString(){
        return nume+" "+String.join("_", compozitie)+" "+String.join("_", indicatii)+" "+String.join("_", contraIndicatii)+" "+modAdministrare+" "+pret+" "+compensatie+" "+nr; 
    }
    public String getInformatii(){
        return "Nume: "+nume+"    "+"Compozitie: "+Arrays.toString(compozitie)+"    "+"Indicatii: "+
                Arrays.toString(indicatii)+"    "+"Contra Indicatii: "+Arrays.toString(contraIndicatii)+"    "+
                "Mod Administrare: "+modAdministrare+"    "+"Pret Vanzare: "+pretVanzare+"   "+
                "Compensat: "+compensatie+"    "+"Numar: "+nr; 
    }  
}
