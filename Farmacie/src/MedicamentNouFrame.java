import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;


class MedicamentNouFrame extends Frame{
     private Button bInscrie;
     private TextField tCompozitie,tIndicatii,tCIndicatii,tModA,tPret;
     private Label lCompozitie,lIndicatii,lCIndicatii,lModA,lPret;
     String nume;
    
    MedicamentNouFrame(String nume){
        super ("Adaugarea unui nou medicament");
        AscultatorCNF acnf=new AscultatorCNF();
        this.nume=nume;
        bInscrie=new Button("Adauga");
        bInscrie.addActionListener(acnf);
       
        tCompozitie=new TextField(20);
        tIndicatii=new TextField(20);
        tCIndicatii=new TextField(20);
        tModA=new TextField(20);
        tPret=new TextField(20);
        lCompozitie=new Label("Compozitie");
        lIndicatii=new Label("Indicatii");
        lCIndicatii=new Label("CIndicatii");
        lModA=new Label("ModA");
        lPret=new Label("Pret");
        
        Panel p1=new Panel();
        p1.setLayout(new GridLayout(5,5));
        p1.add(lCompozitie);p1.add(tCompozitie);
        p1.add(lIndicatii);p1.add(tIndicatii);
        p1.add(lCIndicatii);p1.add(tCIndicatii);
        p1.add(lModA);p1.add(tModA);
        p1.add(lPret);p1.add(tPret);
        
        Panel p2=new Panel();
        p2.add(bInscrie);
        
        add(p1,BorderLayout.NORTH);
        add(p2,BorderLayout.CENTER);
        
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent ev){dispose();}});
    }
    class AscultatorCNF implements ActionListener{
        public void actionPerformed(ActionEvent evt){
                if(evt.getSource()==bInscrie){                
                String compozitie[];
                compozitie=tCompozitie.getText().split(" ");
                String[] indicatii=tIndicatii.getText().split(" ");
                String[] cIndicatii=tCIndicatii.getText().split(" ");
                String modA=tModA.getText();
                String pret=tPret.getText();
                Double pretV=Double.parseDouble(pret);
                if (!compozitie.equals("") && !indicatii.equals("")&& !cIndicatii.equals("")&& !modA.equals("")&& !pret.equals("")){
                    try {
                        Medicament m=new Medicament(nume,compozitie,indicatii,cIndicatii,modA,pretV);
                        ColectieMedicamente.getInstanta().adaugaMedicament(m);
                        dispose();
                    } catch (Exception ex) {
                        Logger.getLogger(MedicamentNouFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else 
                    Mesaj.afiseaza("Eroare la preluarea datelor unui candidat!", "eroare");    
            }
        }
    }
}